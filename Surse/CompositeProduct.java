package com.assignment4.model;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * @author Neagu Tudor 30222
 *
 */
public class CompositeProduct extends ArrayList<MenuItem> implements Serializable, MenuItem {

	private static final long serialVersionUID = 959199801047661090L;
	private String name;
	/**
	 * Constructor
	 * @param name
	 */
	public CompositeProduct(String name) {
		super();//for the arrayList
		this.setName(name);
	}
	
	public CompositeProduct() {
		// TODO Auto-generated constructor stub
	}

	public void setName(String name) {
		this.name = name;
	}
	/**Getter for product name
	 * @return String
	 */
	public String getName() {
		return this.name;
	}

	/**Builds a string with all the names of the MenuItems that compose this product, comma separated
	 * 
	 * @return String
	 */
	public String getIngredientsNames() {
		String string = new String();
		for (MenuItem i : this)
			string += i.getName() + ", ";

		return string.substring(0, string.length() - 2);
	}
	/**
	 * Inherited; Returns total price of the product as the prices sum of the composing products
	 * @return Double
	 */
	@Override
	public Double computePrice() {
		Double price = 0d;
		for (MenuItem i : this)
			price += i.computePrice();

		return price;
	}
	/**A string representation of the object
	 * @return String
	 */
	public String toString() {
		String string = new String();
		string= this.getName();
		return string;
	}

}
