package com.assignment4.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import com.assignment4.data.BillGenerator;
import com.assignment4.presentation.Observer;


public class Restaurant implements Observable, RestaurantProcessing, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1477488982851107492L;

	final String name = "Restaurant";//invariant

	private ArrayList<Order> orders;
	private ArrayList<MenuItem> menuItems;

	public String getName() {
		return name;
	}

	public ArrayList<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(ArrayList<Order> orders) {
		this.orders = orders;
	}

	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(ArrayList<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public Restaurant(ArrayList<Order> orders, ArrayList<MenuItem> menuItems) {
		this.setOrders(orders);
		this.setMenuItems(menuItems);

	}

	public Restaurant() {
		this.orders=new ArrayList<Order>();
		this.menuItems = new ArrayList<MenuItem>();
	}

	@Override
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void updateAllObservers() {
		for (Observer i : observers) {
			i.update();
		}
	}

	@Override
	public void deleteObserver(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public boolean createNewMenuItem(MenuItem menuItem) {
		assert menuItem != null : "MenuItem is null!";
		return menuItems.add(menuItem);

	}

	@Override
	public boolean deleteMenuItem(MenuItem menuItem) {
		return menuItems.remove(menuItem);
	}

	public MenuItem menuListContains(String name) {
		for (MenuItem i : menuItems)
			if (i instanceof CompositeProduct && ((CompositeProduct) i).getName().equals(name))
				return i;
			else if (i instanceof BaseProduct && ((BaseProduct) i).getName().equals(name))
				return i;
		
		return null;
	}

	@Override
	
	public boolean createNewOrder(Order order) {
		assert order != null : "Order is null!";
		orders.add(order);
		
		
		this.updateAllObservers();
		return true;
	}

	@Override
	public Double computePrice(Order order) {
		return order.computePrice();
	}

	@Override
	public void generateBill(Order order) {
		assert order != null : "Order is null!";
		try {
			BillGenerator.createNewBill(order.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	

}
