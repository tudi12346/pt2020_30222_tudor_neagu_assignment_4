package com.assignment4.presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.assignment4.data.BillGenerator;
import com.assignment4.model.MenuItem;
import com.assignment4.model.Order;
import com.assignment4.model.Restaurant;

public class WaiterWindow {

	private JFrame frame;
	private JTable table;
	JScrollPane scrollPane;
	JScrollPane scrollPane_1;
	private Restaurant r;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public void launch(Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WaiterWindow window = new WaiterWindow(r);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void refreshMenu() {
		String[] columns = new String[] { "Name", "Price" };
		scrollPane.remove(table);
		frame.remove(scrollPane);
		frame.repaint();

		scrollPane = new JScrollPane();
		scrollPane.setBounds(223, 66, 453, 138);
		frame.getContentPane().add(scrollPane);

		// add the table to the frame
		Object[][] data = new Object[r.getMenuItems().size()][2];

		int k = 0;

		for (MenuItem i : r.getMenuItems()) {

			data[k][0] = i.getName();
			data[k][1] = i.computePrice();

			k++;
		}
		// create table with data

		table = new JTable(data, columns);

		scrollPane.setViewportView(table);
		
		
	}

	public void refreshOrders() {
		String[] columnsO = new String[] { "OrderId", "TableID", "Items", "Date" };

		scrollPane_1.remove(table_1);
		frame.remove(scrollPane_1);
		frame.repaint();
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(223, 243, 453, 173);
		frame.getContentPane().add(scrollPane_1);
		
		int k = 0;
		Object[][] dataOnew = new Object[r.getOrders().size()][4];
		for (Order or : r.getOrders()) {

			dataOnew[k][0] = or.getId().toString();
			dataOnew[k][1] = or.getTableId().toString();
			dataOnew[k][2] = or.getMenuItems().toString();
			dataOnew[k][3] = or.getDate();

			k++;
		}
		// create table with data

		table_1 = new JTable(dataOnew, columnsO);


		
		scrollPane_1.setViewportView(table_1);
	}
	/**
	 * Create the application.
	 */
	public WaiterWindow(Restaurant r) {
		this.r = r;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		frame = new JFrame("Waiter Window");
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(223, 243, 453, 173);
		frame.getContentPane().add(scrollPane_1);
		String[] columnsO = new String[] { "OrderId", "TableID", "Items", "Date" };

		// add the table to the frame
		int k = 0;
		Object[][] dataO = new Object[r.getOrders().size()][4];
		for (Order o : r.getOrders()) {

			dataO[k][0] = o.getId().toString();
			dataO[k][1] = o.getTableId().toString();
			dataO[k][2] = o.getMenuItems().toString();
			dataO[k][3] = o.getDate();

			k++;
		}
		table_1 = new JTable(dataO, columnsO);
		scrollPane_1.setViewportView(table_1);

		String[] columns = new String[] { "Name", "Price" };

		// add the table to the frame
		Object[][] data = new Object[r.getMenuItems().size()][2];

		k = 0;

		for (MenuItem i : r.getMenuItems()) {

			data[k][0] = i.getName();
			data[k][1] = i.computePrice();

			k++;
		}
		// create table with data

		table = new JTable(data, columns);

		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(223, 66, 453, 138);
		frame.getContentPane().add(scrollPane);

		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(0, 255, 153));
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(208, 10, 5, 430);
		frame.getContentPane().add(separator);

		String[] tables = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
		JComboBox comboBox = new JComboBox(tables);
		comboBox.setBounds(28, 255, 36, 21);
		frame.getContentPane().add(comboBox);

		JLabel lblNewLabel = new JLabel("TableID");
		lblNewLabel.setBounds(28, 235, 45, 13);
		frame.getContentPane().add(lblNewLabel);

		JButton btnNewButton = new JButton("New Order");
		btnNewButton.setBounds(28, 122, 135, 21);
		frame.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int[] selected = table.getSelectedRows();
				if (selected.length != 0) {
					Order o = new Order(Integer.parseInt((String) comboBox.getSelectedItem()));
				
					String itemName;
					for (int i : selected) {
						itemName = (String) table.getValueAt(i, 0);
						o.getMenuItems().add(r.menuListContains(itemName));
					}
					
					r.createNewOrder(o);
					for (MenuItem i : o.getMenuItems()) {
						System.out.println(i.getName() + " ");
					}
					refreshOrders();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "No items selected!","Error", JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		JButton btnNewButton_1 = new JButton("Bill Order");
		btnNewButton_1.setBounds(28, 168, 135, 21);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selected = table_1.getSelectedRows();
				Order o = null;

				for (int i : selected) {
					o = r.getOrders().get(i);
					break;
				}
				if (o != null) {
					try {
						BillGenerator.createNewBill(o.toString() + "\ntotal price =" + o.computePrice().toString());
					} catch (IOException e1) {
						
						e1.printStackTrace();
					}
				} else {
					JOptionPane.showMessageDialog(null, "Could not find any registered order","Error", JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		frame.getContentPane().add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("RefreshMenu");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMenu();
			}
		});
		btnNewButton_2.setBounds(28, 37, 135, 21);
		frame.getContentPane().add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("RefreshOrders");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshOrders();

			}
		});
		btnNewButton_3.setBounds(28, 80, 135, 21);
		frame.getContentPane().add(btnNewButton_3);

	}
}
