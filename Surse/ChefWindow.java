package com.assignment4.presentation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.assignment4.model.Order;
import com.assignment4.model.Restaurant;

public class ChefWindow implements Observer{

	private JFrame frmChefwindow;
	JScrollPane orderScrollPane;
	private Restaurant r;
	private JTable table;
	/**
	 * Launch the application.
	 */
	public void launch(Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChefWindow window = new ChefWindow(r);
					window.frmChefwindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ChefWindow(Restaurant r) {
		this.r = r;
		r.addObserver(this);
		initialize();
	}
	
	
	public void refreshOrders() {
		String[] columnsO = new String[] { "OrderId", "TableID", "Items", "Date" };

		orderScrollPane.remove(table);
		frmChefwindow.remove(orderScrollPane);
		frmChefwindow.repaint();
		
		orderScrollPane = new JScrollPane();
		orderScrollPane.setBounds(223, 243, 453, 173);
		frmChefwindow.getContentPane().add(orderScrollPane);
		
		int k = 0;
		Object[][] dataOnew = new Object[r.getOrders().size()][4];
		for (Order or : r.getOrders()) {

			dataOnew[k][0] = or.getId().toString();
			dataOnew[k][1] = or.getTableId().toString();
			dataOnew[k][2] = or.getMenuItems().toString();
			dataOnew[k][3] = or.getDate();

			k++;
		}
		// create table with data

		table = new JTable(dataOnew, columnsO);


		
		orderScrollPane.setViewportView(table);
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmChefwindow = new JFrame();
		frmChefwindow.setTitle("ChefWindow");
		frmChefwindow.setBounds(100, 100, 500, 399);
		frmChefwindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		
		String[] columnsO = new String[] { "OrderId", "TableID", "Items", "Date" };
		orderScrollPane = new JScrollPane();
		orderScrollPane.setBounds(0, 0, 453, 173);
		frmChefwindow.getContentPane().add(orderScrollPane);
		
		int k = 0;
		Object[][] dataOnew = new Object[r.getOrders().size()][4];
		for (Order or : r.getOrders()) {

			dataOnew[k][0] = or.getId().toString();
			dataOnew[k][1] = or.getTableId().toString();
			dataOnew[k][2] = or.getMenuItems().toString();
			dataOnew[k][3] = or.getDate();

			k++;
		}
		// create table with data

		table = new JTable(dataOnew, columnsO);
		orderScrollPane.setViewportView(table);
		
		
		
	}

	@Override
	public void update() {
		String[] columnsO = new String[] { "OrderId", "TableID", "Items", "Date" };

		orderScrollPane.remove(table);
		frmChefwindow.remove(orderScrollPane);
		frmChefwindow.repaint();
		
		orderScrollPane = new JScrollPane();
		orderScrollPane.setBounds(0, 0, 500, 399);
		frmChefwindow.getContentPane().add(orderScrollPane);
		
		int k = 0;
		Object[][] dataOnew = new Object[r.getOrders().size()][4];
		for (Order or : r.getOrders()) {

			dataOnew[k][0] = or.getId().toString();
			dataOnew[k][1] = or.getTableId().toString();
			dataOnew[k][2] = or.getMenuItems().toString();
			dataOnew[k][3] = or.getDate();

			k++;
		}
		// create table with data

		table = new JTable(dataOnew, columnsO);


		
		orderScrollPane.setViewportView(table);
		
	}

}
