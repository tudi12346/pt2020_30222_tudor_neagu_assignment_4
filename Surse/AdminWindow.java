package com.assignment4.presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.assignment4.model.BaseProduct;
import com.assignment4.model.CompositeProduct;
import com.assignment4.model.MenuItem;
import com.assignment4.model.Restaurant;

public class AdminWindow {

	private JFrame frame;
	private JTable baseTable;
	JScrollPane basePane;
	JScrollPane compositePane;
	private Restaurant r;
	private JTable compositeTable;
	private JTextField baseProductNameField;
	private JTextField baseProductPriceField;
	private JTextField compositeProductField;

	/**
	 * Launch the application.
	 */
	public void launch(Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminWindow window = new AdminWindow(r);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminWindow(Restaurant r) {
		this.r = r;
		initialize();
	}

	public void refreshCompositeTable() {
		String[] compositeColumns = new String[] { "Name", "Ingredients", "Price" };
		compositePane.remove(compositeTable);
		frame.remove(compositePane);
		frame.repaint();

		compositePane = new JScrollPane();
		compositePane.setBounds(223, 243, 453, 173);
		frame.getContentPane().add(compositePane);

		int rowCount = 0;
		for (MenuItem i : r.getMenuItems()) {
			if (i instanceof CompositeProduct)
				rowCount++;
		}
		int k = 0;
		Object[][] compositeData = new Object[rowCount][3];
		for (MenuItem i : r.getMenuItems()) {
			CompositeProduct temp;
			if (i instanceof CompositeProduct) {
				temp = (CompositeProduct) i;
				compositeData[k][0] = temp.getName();
				compositeData[k][1] = temp.getIngredientsNames();
				compositeData[k][2] = temp.computePrice().toString();

				k++;
			}
		}
		compositeTable = new JTable(compositeData, compositeColumns);
		compositePane.setViewportView(compositeTable);

	}

	public void refreshBaseTable() {
		String[] baseColumns = new String[] { "Name", "Price" };
		basePane.remove(baseTable);
		frame.remove(basePane);
		frame.repaint();

		basePane = new JScrollPane();
		basePane.setBounds(223, 66, 453, 138);
		frame.getContentPane().add(basePane);

		int rowCount = 0;
		for (MenuItem i : r.getMenuItems()) {
			if (i instanceof BaseProduct)
				rowCount++;
		}
		// add the baseTable to the frame

		Object[][] baseData = new Object[rowCount][2];

		int k = 0;

		for (MenuItem i : r.getMenuItems()) {

			if (i instanceof BaseProduct) {
				baseData[k][0] = i.getName();
				baseData[k][1] = i.computePrice();

				k++;
			}

		}
		baseTable = new JTable(baseData, baseColumns);
		basePane.setViewportView(baseTable);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Admin Window");
		frame.setBounds(100, 100, 700, 500);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// composite products table
		compositePane = new JScrollPane();
		compositePane.setBounds(223, 243, 453, 173);
		frame.getContentPane().add(compositePane);
		String[] compositeColumns = new String[] { "Name", "Ingredients", "Price" };

		// add the baseTable to the frame
		int rowCount = 0;
		for (MenuItem i : r.getMenuItems()) {
			if (i instanceof CompositeProduct)
				rowCount++;
		}
		int k = 0;
		Object[][] compositeData = new Object[rowCount][3];
		for (MenuItem i : r.getMenuItems()) {
			CompositeProduct temp;
			if (i instanceof CompositeProduct) {
				temp = (CompositeProduct) i;
				compositeData[k][0] = temp.getName();
				compositeData[k][1] = temp.getIngredientsNames();
				compositeData[k][2] = temp.computePrice().toString();

				k++;
			}
		}
		compositeTable = new JTable(compositeData, compositeColumns);
		compositePane.setViewportView(compositeTable);

		// base products table

		basePane = new JScrollPane();
		basePane.setBounds(223, 66, 453, 138);
		frame.getContentPane().add(basePane);
		String[] baseColumns = new String[] { "Name", "Price" };
		rowCount = 0;
		for (MenuItem i : r.getMenuItems()) {
			if (i instanceof BaseProduct)
				rowCount++;
		}
		// add the baseTable to the frame
		Object[][] baseData = new Object[rowCount][2];

		k = 0;

		for (MenuItem i : r.getMenuItems()) {

			if (i instanceof BaseProduct) {
				baseData[k][0] = i.getName();
				baseData[k][1] = i.computePrice();

				k++;
			}

		}
		// create baseTable with data

		baseTable = new JTable(baseData, baseColumns);
		basePane.setViewportView(baseTable);

		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(0, 255, 153));
		separator.setOrientation(SwingConstants.VERTICAL);
		separator.setBounds(208, 10, 5, 430);
		frame.getContentPane().add(separator);

		JLabel baseProductsLabel = new JLabel("BaseProducts");
		baseProductsLabel.setBounds(223, 43, 101, 13);
		frame.getContentPane().add(baseProductsLabel);

		JLabel compositeProductsLabel = new JLabel("CompositeProducts");
		compositeProductsLabel.setBounds(223, 220, 114, 13);
		frame.getContentPane().add(compositeProductsLabel);

		baseProductNameField = new JTextField();
		baseProductNameField.setBounds(10, 40, 120, 19);
		frame.getContentPane().add(baseProductNameField);
		baseProductNameField.setColumns(10);

		baseProductPriceField = new JTextField();
		baseProductPriceField.setBounds(10, 90, 120, 19);
		frame.getContentPane().add(baseProductPriceField);
		baseProductPriceField.setColumns(10);

		JLabel baseProductNameLabel = new JLabel("BaseProductName");
		baseProductNameLabel.setBounds(10, 17, 120, 13);
		frame.getContentPane().add(baseProductNameLabel);

		JLabel baseProductPriceLabel = new JLabel("BaseProductPrice");
		baseProductPriceLabel.setBounds(10, 73, 120, 13);
		frame.getContentPane().add(baseProductPriceLabel);

		JButton newBaseProductButton = new JButton("NewBaseProd");
		newBaseProductButton.setBounds(10, 135, 120, 21);
		frame.getContentPane().add(newBaseProductButton);

		compositeProductField = new JTextField();
		compositeProductField.setBounds(10, 247, 120, 19);
		frame.getContentPane().add(compositeProductField);
		compositeProductField.setColumns(10);

		JLabel compositeProductNameLabel = new JLabel("CompProductName");
		compositeProductNameLabel.setBounds(10, 224, 120, 13);
		frame.getContentPane().add(compositeProductNameLabel);

		JButton newCompositeProductButton = new JButton("NewCompProd");
		newCompositeProductButton.setBounds(10, 292, 120, 21);
		frame.getContentPane().add(newCompositeProductButton);

		JButton updateBaseProductButton = new JButton("UpdateBasePr");
		updateBaseProductButton.setBounds(10, 197, 120, 21);
		frame.getContentPane().add(updateBaseProductButton);

		JButton deleteCompositeProductButton = new JButton("DeleteCompPr");
		deleteCompositeProductButton.setBounds(10, 323, 120, 21);
		frame.getContentPane().add(deleteCompositeProductButton);

		JButton deleteBaseProductButton = new JButton("DeleteBasePr");
		deleteBaseProductButton.setBounds(10, 166, 120, 21);
		frame.getContentPane().add(deleteBaseProductButton);

		JButton updateCompositeProductButton = new JButton("UpdateCompPr");
		updateCompositeProductButton.setBounds(10, 354, 120, 21);
		frame.getContentPane().add(updateCompositeProductButton);

		newCompositeProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int[] selected = baseTable.getSelectedRows();
				String itemName = compositeProductField.getText();
				if (selected.length != 0 && !itemName.isBlank() && !itemName.isEmpty()) {

					CompositeProduct prod = new CompositeProduct(itemName);
					for (int i : selected) {
						itemName = (String) baseTable.getValueAt(i, 0);
						prod.add(r.menuListContains(itemName));
					}

					r.createNewMenuItem(prod);
					for (MenuItem i : prod) {
						System.out.println(i.getName() + " ");
					}

					refreshCompositeTable();

				} else {
					JOptionPane.showMessageDialog(null, "Invalid name or no items selected!", "Error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		newBaseProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = baseProductNameField.getText();
				String priceString = baseProductPriceField.getText();

				if (!name.isBlank() && !name.isEmpty() && !priceString.isEmpty() && !priceString.isBlank()) {

					Double price = Double.parseDouble(priceString);
					BaseProduct prod = new BaseProduct(price, name);
					r.getMenuItems().add(prod);

					refreshBaseTable();

				}
			}
		});

		deleteBaseProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] selected = baseTable.getSelectedRows();

				if (selected.length != 0) {
					for (int i : selected) {
						BaseProduct prod = (BaseProduct) r.menuListContains((String) baseTable.getValueAt(i, 0));
						r.deleteMenuItem(prod);
					}

					refreshBaseTable();

				} else {
					JOptionPane.showMessageDialog(null, "No items selected!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		deleteCompositeProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] selected = compositeTable.getSelectedRows();

				if (selected.length != 0) {
					for (int i : selected) {
						CompositeProduct prod = (CompositeProduct) r
								.menuListContains((String) compositeTable.getValueAt(i, 0));
						r.deleteMenuItem(prod);
					}

					refreshCompositeTable();

				} else {
					JOptionPane.showMessageDialog(null, "No items selected!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		updateBaseProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] selected = baseTable.getSelectedRows();
				String name = baseProductNameField.getText();
				String priceString = baseProductPriceField.getText();

				if (selected.length != 0 && !name.isBlank() && !name.isEmpty() && !priceString.isEmpty()
						&& !priceString.isBlank()) {
					for (int i : selected) {
						BaseProduct prod = (BaseProduct) r.menuListContains((String) baseTable.getValueAt(i, 0));
						prod.setName(baseProductNameField.getText());
						prod.setPrice(Double.parseDouble(baseProductPriceField.getText()));
					}

					refreshBaseTable();
					refreshCompositeTable();

				} else {
					JOptionPane.showMessageDialog(null, "No items selected or invalid new name or price!", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		updateCompositeProductButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int[] selected = baseTable.getSelectedRows();
				String itemName = compositeProductField.getText();
				if (selected.length != 0 && !itemName.isBlank() && !itemName.isEmpty()) {

					int[] selectedComposite = compositeTable.getSelectedRows();
					CompositeProduct prod = null;
					if (selectedComposite.length != 0) {

						prod = (CompositeProduct) r.menuListContains((String) compositeTable.getValueAt(selectedComposite[0], 0));

					} else
						return;
					if(prod!=null)
					{
						prod.setName(itemName);
						prod.clear();
					}

					for (int i : selected) {
						itemName = (String) baseTable.getValueAt(i, 0);
						prod.add(r.menuListContains(itemName));
					}
					
					
					for (MenuItem i : prod) {
						System.out.println(i.getName() + " ");
					}

					refreshCompositeTable();

				} else {
					JOptionPane.showMessageDialog(null, "Invalid name or no items selected!", "Error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});

	}
}