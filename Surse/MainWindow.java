package com.assignment4.presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.assignment4.data.RestaurantSerializer;
import com.assignment4.model.Restaurant;

public class MainWindow {

	private JFrame frame;
	private Restaurant r;
	/**
	 * Launch the application.
	 */
	public void launch(Restaurant r) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow(r);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow(Restaurant r) {
		this.r=r;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("RestaurantManagement");
		frame.setBounds(100, 100, 324, 289);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		
		JButton waiterWindowButton = new JButton("Waiter");
		waiterWindowButton.setBounds(10, 35, 85, 50);
		frame.getContentPane().add(waiterWindowButton);
		
		JButton adminWindowButton = new JButton("Admin");
		adminWindowButton.setBounds(95, 95, 85, 50);
		frame.getContentPane().add(adminWindowButton);
		
		JButton chefWindowButton = new JButton("Chef");
		chefWindowButton.setBounds(180, 155, 85, 50);
		frame.getContentPane().add(chefWindowButton);
		
		adminWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdminWindow admin = new AdminWindow(r);
				admin.launch(r);
			}
		});
		
		waiterWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WaiterWindow ww = new WaiterWindow(r);
				ww.launch(r);

			}
		});
		
		chefWindowButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ChefWindow chef = new ChefWindow(r);
				chef.launch(r);

			}
		});
		
		frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                RestaurantSerializer.serialize(r);
                e.getWindow().dispose();
            }
        });
	}
	

}
