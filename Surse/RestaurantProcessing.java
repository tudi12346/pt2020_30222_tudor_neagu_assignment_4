package com.assignment4.model;
/**
 * 
 * @author Neagu Tudor
 *
 */
public interface RestaurantProcessing {
	/**Method to add a new item to the menu of the restaurant;
	 *  if item is null, do not create, else create
	 * @param menuItem
	 * @return true if added successfully, false otherwise
	 */
	public boolean createNewMenuItem(MenuItem menuItem);
	
	/**Method to delete an item from the menu of the restaurant;
	 * 
	 * @param menuItem
	 * @return true if deleted successfully, false otherwise
	 */
	public boolean deleteMenuItem(MenuItem menuItem);
	
	/**Method to add a new order to the list of orders of the restaurant;
	 * If order is not null, add it, else don t
	 * @param order
	 * @return true if added successfully, false otherwise
	 */
	public boolean createNewOrder(Order order);
	
	/**Method to compute the total price of an order;
	 * If order is not null, compute the price, else don t
	 * @param order
	 * @return double value representing the price
	 */
	public Double computePrice(Order order);
	
	/**Generates .txt bill for an order;
	 * If order is not null, generate bill, else don 't
	 * @param order
	 */
	public void generateBill(Order order);
}
