package com.assignment4.model;

import java.io.Serializable;

public class BaseProduct implements MenuItem, Serializable {
	
	private static final long serialVersionUID = -2794889118278569211L;
	private Double price;
	private String name;
	/**
	 * Constructor
	 * @param price
	 * @param name
	 */
	public BaseProduct(Double price, String name) {
		this.setPrice(price);
		this.setName(name);
	}
	/**
	 * Product price
	 * @return Double
	 */
	public Double getPrice() {
		return price;
	}
	/**
	 * Setter for product price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
	/**
	 * Getter for product name
	 * @return String 
	 */
	public String getName() {
		return name;
	}
	/**
	 * Setter for product name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * String representation of the object
	 * @return String
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	/**
	 * Inherited method for computing final price; in this case just returns price
	 * @return Double
	 */
	@Override
	public Double computePrice() {
		return this.getPrice();
	}

}
