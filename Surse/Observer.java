package com.assignment4.presentation;
/**
 * 
 * @author Neagu Tudor 30222
 *
 */
public interface Observer {
	void update();
}
