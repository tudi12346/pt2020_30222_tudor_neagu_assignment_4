package com.assignment4.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Order implements Serializable {

	private static final long serialVersionUID = 3165724144403229736L;
	static Integer increment = 0;
	private Integer id;
	private Integer tableId;
	private String date;

	private ArrayList<MenuItem> menuItems;

	public Order(int tableId) {
		this.id = ++increment;
		this.setTableId(tableId);
		this.setDate(    new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date()));

		menuItems = new ArrayList<MenuItem>();
	}

	public Order() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(ArrayList<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public boolean addMenuItem(MenuItem menuItem) {
		if (menuItems.add(menuItem))
			return true;
		else
			return false;
	}

	@Override
	public String toString() {
		String str = new String();
		for(MenuItem i : menuItems)
		{
			str += i.toString()+" price = "+ i.computePrice().toString()+"\n";
		}
		return str;
	}

	public Double computePrice() {
		Double price = 0d;
		for (MenuItem i : menuItems)
			price += i.computePrice();
		return price;
	}
}
