package com.assignment4.data;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BillGenerator {
	public static String createNewBill(String bill) throws IOException {
		String fileName = new String("Bills/Bill_");
		fileName += new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
		fileName += ".txt";
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		writer.write(bill);
		writer.close();
		return fileName;
	}
}
