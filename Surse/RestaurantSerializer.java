package com.assignment4.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.assignment4.model.Restaurant;

public class RestaurantSerializer {
	static final String restaurantSerPath = "Data/Restaurant.ser";

	public static void serialize(Restaurant restaurant) {
		
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(restaurantSerPath);
			ObjectOutputStream objectOutputStrean = new ObjectOutputStream(fileOutputStream);

			objectOutputStrean.writeObject(restaurant);
			objectOutputStrean.close();
			fileOutputStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static Restaurant deserialize() {
		
		Restaurant res = null ;
		try {
			FileInputStream fileInputStream = new FileInputStream(restaurantSerPath);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

			res = ((Restaurant) objectInputStream.readObject());

			objectInputStream.close();
			fileInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return res;
	}
}