package com.assignment4.model;

import java.util.ArrayList;

import com.assignment4.presentation.Observer;
/**
 * 
 * @author Neagu Tudor 30222
 *
 */
public interface Observable {
	public final ArrayList<Observer> observers = new ArrayList<Observer>();
	
	void addObserver(Observer observer);
	void updateAllObservers();
	void deleteObserver(Observer observer);
}
