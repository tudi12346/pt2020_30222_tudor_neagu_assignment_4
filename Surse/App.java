package com.assignment4;

import com.assignment4.data.RestaurantSerializer;
import com.assignment4.model.Restaurant;
import com.assignment4.presentation.MainWindow;

public class App {

	public static void main(String[] args) {
//		Restaurant r = new Restaurant(null, null);
//		ArrayList<Order> orders= new ArrayList<Order>();
//		ArrayList<MenuItem> menu= new ArrayList<MenuItem>();
//		ArrayList<MenuItem> men= new ArrayList<MenuItem>();
//		Order o = new Order(2);
//		men.add(new BaseProduct(1d, "branza"));
//		o.setMenuItems(men);
//		orders.add(o);
//		
//		r.setOrders(orders);
//		r.setMenuItems(menu);
//		
//		RestaurantSerializer.serialize(r);
//		
//		Restaurant rr = RestaurantSerializer.deserialize();
//		System.out.println(rr.getOrders().get(0).getMenuItems().get(0).getName());
		
//		ArrayList<Order> orders= new ArrayList<Order>();
//		ArrayList<MenuItem> menuItems= new ArrayList<MenuItem>();
//		BaseProduct p = new BaseProduct(12d, "lapte");
//		BaseProduct q = new BaseProduct(13d, "miere");
//		BaseProduct s = new BaseProduct(10d, "smochine");
//		
//		BaseProduct a = new BaseProduct(10d, "cirese");
//		BaseProduct b = new BaseProduct(10d, "branza");
//		CompositeProduct pro = new CompositeProduct("ceai");
//		
//		menuItems.add(p);
//		menuItems.add(q);
//		menuItems.add(s);
//		menuItems.add(a);
//		menuItems.add(b);
//		pro.add(a);
//		pro.add(b);
//		menuItems.add(pro);
//		Restaurant r = new Restaurant(orders,menuItems);
		Restaurant r = RestaurantSerializer.deserialize();
		if(r == null)
			r=new Restaurant();
		MainWindow m = new MainWindow(r);
		m.launch(r);

		
	}

}
