package com.assignment4.model;
/**
 * @author Neagu Tudor 30222
 *
 */
public interface MenuItem {
	/**
	 * total price of 1 unit
	 * @return Double
	 */
	public Double computePrice();
	/**
	 * String representation of the object
	 * @return String 
	 */
	public String toString();
	/**
	 * name of the Item
	 * @return String
	 */
	public String getName();
}
